# My Retro Game

This is only a practice art that I have created when I was learning how to make my own games. Using JavaScript, I added the version of a skeletal game. 

Don’t worry if this game looks crappy, but this is the [outdoor wall art](https://www.soothingcompany.com/outdoor/outdoor-canvas-art.html) of my life. I have created the skeletal version of Asteroids, lunar lander, space invader and snake. I know everyone already played this game, and I’m too magnificent to show my art using the JavaScript XD See the photos below: 


![Screenshot of Asteroids](asteroids/screenshot.png)

![Screenshot of Lunar Lander](lunar-lander/screenshot.png)

![Screenshot of Space Invaders](space-invaders/screenshot.png)

![Screenshot of Snake](snake/screenshot.png)